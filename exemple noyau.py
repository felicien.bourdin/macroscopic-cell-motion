import numpy as np
import matplotlib.pyplot as plt
def noyau_interaction(u,rPot):
    if u <= rPot:
        return rPot**2*35/32*((1-u**2/rPot**2)**3)
    else:
        return 0

XX = np.linspace(-1,1,200)
YY=np.linspace(-1,1,200)
r=.2
for i in range(200):
    if ((i-100)/100/r)**2<1:
        print(((i-100)/100/r)**2)
        YY[i] = (1-((i-100)/100/r)**2)**3
    else:
        YY[i]=0

plt.plot(XX,YY)
plt.show()