Project realized by Félicien BOURDIN during his PhD Thesis.
The work of Félicien BOURDIN is supported by the ERC Grant NORIA.

# **A ready-to-use implementation is available in**

```demo.ipynb```

# Presentation of the Splitting Scheme


`Adaptation of the splitting scheme detailled in section 4.2 of [https://arxiv.org/pdf/1101.4102v1.pdf](url) in the case of a population splitted into two types.

The step of the scheme is the following. Start from two densities <img src="https://latex.codecogs.com/gif.latex?\rho_1,\rho_2\in W_2(\Omega) " /> supported by an open set in 2D, satisfying <img src="https://latex.codecogs.com/gif.latex?\rho_1+\rho_2\leq 1 " /> .

**Step 1: transport (Pshfwd.py)**

Transport the densities by the desired velocities  <img src="https://latex.codecogs.com/gif.latex?U_1,U_2\in \mathbb{L}_2(\Omega,\mathbb{R}^2)^2 " />  during a timestep <img src="https://latex.codecogs.com/gif.latex?\tau" /> : 
<img src="https://latex.codecogs.com/gif.latex?\mu_i = (Id+\tau U_i)_\sharp \rho_i " /> .
Two possibilities: finite volume or lagrangian transport.

**Step 2: projection (K2_proj.py, Sinkhorn.py)**

Project the sum of the intermediate densities <img src="https://latex.codecogs.com/gif.latex?\mu_1+\mu_2" /> on the set of admissible densities <img src="https://latex.codecogs.com/gif.latex?K_1 = \left\{\nu \in W_2(\Omega),\, \nu \leq 1 \right\}" /> with the stochastic scheme presented in [https://arxiv.org/pdf/1101.4102v1.pdf](url).

Compute the transport plan <img src="https://latex.codecogs.com/gif.latex?T" /> from <img src="https://latex.codecogs.com/gif.latex?\mu_1+\mu_2" /> to its projection on <img src="https://latex.codecogs.com/gif.latex?K_1" />, and compute the projection of <img src="https://latex.codecogs.com/gif.latex?(\mu_1,\mu_2)" /> on <img src="https://latex.codecogs.com/gif.latex?K_2 = \left\{\nu_1,\nu_2 \in W_2(\Omega),\, \nu_1+\nu_2 \leq 1 \right\}" /> with
<img src="https://latex.codecogs.com/gif.latex?\nu_i = T_\sharp \mu_i" />.

Then set <img src="https://latex.codecogs.com/gif.latex?\rho_i = \mu_i" />.

# How to run

One inputs


**Global parameters**

    N - meshsize

    Ntest - Number of parallel runs of the stochastic projection

    dt - timestep
    
    Ttot - total time

**Method parameters**

    epsilon  - Regularisation parameter in Sinkhorn (optional, default 0.0001)

    niter    - Max number of iterations in Sinkhorn (optional, default 10)

    key_proj = 'stoch'  - Choice of the projection method (only stoch is available for now) (optional, default 'stoch')

    key_pshfwd - Choice of the PshFwd scheme ('lagrangian' or 'volfin') (optional, default "lagrangian")

    Ntest - Number of parallel runs of the stochastic projection (optional, default 50)


**Model parameters**

    rho_1, rho_2 - initial densities


    Desired velocity
        zeta - External velocity field parameter (optional, default 1)
        u_1, u_2 - desired velocities

    Chemoattraction (optional)
        xi - Chemoattraction parameter
        alpha - Inter-type chemoattraction coefficient parameter (for equal attraction regardless of the type: 1)

    External potential (optional)
        chi - External potential parameter
        pot_1, pot_2 - external potentials

    Interaction (optional)
        eta - Interaction parameter
        beta - Inter-type interaction parameter  (for equal interaction regardless of the type: 1)
        rPot - Radius of interaction (the space is [0,1]*[0,1])
        kernel - interaction kernel








The general desired velocity is there

<img src="https://latex.codecogs.com/gif.latex?U_i = \chi \nabla c - \xi \nabla pot_i + \eta \nabla(V\ast (\rho_1+\rho_2))" /> with <img src="https://latex.codecogs.com/gif.latex?V(x,y) = \mathrm{noyau_{interaction}}(\lVert x - y \rVert)" />

The results are stored in the folder img_vid/last_run.
