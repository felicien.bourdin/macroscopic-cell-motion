import numpy as np
import matplotlib
from math import *
import matplotlib.pyplot as plt
import time
import torch
from torch.autograd import Variable
from mpl_toolkits.axes_grid1 import make_axes_locatable

import Diff_mat
import Sinkhorn
import K2_proj
import Pshfwd
import LagrangianPshFwd

"""

The code does not work if every velocity is null with the finite volume transport (maybe an issue in the discretization of the upwind operator).
Every timestep is the composition of 3 steps:

Step 1: transport of the densities by the desired velocities (2 choices: finite volume or lagrangian method),
Step 2: the sum of the obtained densities is projected on K1 the set of admissible densities (2 choices: stochastic projection or sandpile) - SANDPILE UNAVAILABLE,
Step 3: With Sinkhorn's algorithm, the global transport plan is computed and we deduce the separed projected densities.

The results are plotted and saved img_vid/last_run

"""

def Splitting(rho_1,rho_2,N, dt, Iterations, u_1, u_2, Ntest=50, epsilon=0.0001, niter=10, key_proj="stoch",
              key_pshfwd="lagrangian", store_results = False, draw_results=True, **kwargs):

    start_time = time.time()
    external_pot = False
    chemoattr = False
    interaction = False

    zeta = 1

    for key in kwargs:

        if key == "pot_1" or key == "pot_1" or key == "xi":
            ## Detection of external potential
            external_pot = True
            try:
                pot_1 = kwargs["pot_1"]
                pot_2 = kwargs["pot_2"]
                xi = kwargs["xi"]
            except:
                raise ValueError("if external potential is used, specify pot_1,pot_2 and xi")

        if key == "chi" or key == "alpha":
            ## Detection of chemoattraction
            chemoattr = True
            try:
                chi = kwargs["chi"]
                alpha = kwargs["alpha"]
            except:
                raise ValueError("if chemoattraction is used, specify chi and alpha")

        if key == "zeta":
            zeta = kwargs["zeta"]

        if key == "eta" or key == "beta" or key == "kernel":
            interaction = True
            try:
                eta = kwargs["eta"]
                beta = kwargs["beta"]
                kernel = kwargs["kernel"]
                rPot = kwargs["rPot"]
            except:
                raise ValueError("if interaction is used, specify eta, beta and kernel")

    rho_1_i = rho_1
    rho_2_i = rho_2

    if store_results:
        stock = [[rho_1_i.T+rho_2_i.T,rho_1_i.T,rho_2_i.T]]

    for s in range(Iterations):

        if s != 0:
            rho_1_i = rho_1_f
            rho_2_i = rho_2_f
        print('Iteration ', s, ' sur ', Iterations, " Temps : %s secondes ---" % (time.time() - start_time))

        ## --------------- STEP 1 : PUSHFWD ------------------

        # Compute the potentials

        if external_pot:
            pot_fixe = xi * Diff_mat.to_mat(pot_1, N), xi * Diff_mat.to_mat(pot_2, N)
        else:
            pot_fixe = np.zeros_like(rho_1)

        if chemoattr:
            pot_var_1 = chi * Diff_mat.pot_chemo(rho_1_i, alpha * rho_2_i, N)[0]
            pot_var_2 = chi * Diff_mat.pot_chemo(alpha * rho_1_i, rho_2_i, N)[1]
        else:
            pot_var_1 = np.zeros_like(rho_1)
            pot_var_2 = np.zeros_like(rho_1)

        if interaction:
            pot_inter_1 = Diff_mat.mat_interaction(rho_1_i, rho_2_i * 0., kernel, rPot)
            pot_inter_2 = Diff_mat.mat_interaction(rho_1_i * 0., rho_2_i, kernel, rPot)

            pot_inter_11 = eta * pot_inter_1
            pot_inter_12 = beta * eta * pot_inter_2

            pot_inter_21 = eta * pot_inter_2
            pot_inter_22 = beta * eta * pot_inter_1

        else:
            pot_inter_11 = np.zeros_like(rho_1)
            pot_inter_12 = np.zeros_like(rho_1)
            pot_inter_21 = np.zeros_like(rho_1)
            pot_inter_22 = np.zeros_like(rho_1)

        potentiel_1 = pot_fixe[0] - pot_var_1 - pot_inter_11 - pot_inter_12
        potentiel_2 = pot_fixe[1] - pot_var_2 - pot_inter_21 - pot_inter_22
        flux_vitesses = Pshfwd.flux(u_1, u_2, potentiel_1, potentiel_2, N, zeta)

        if key_pshfwd == "volfin":
            rho_1_i = Pshfwd.pas_fwd(rho_1_i, dt, 1, flux_vitesses)
            rho_2_i = Pshfwd.pas_fwd(rho_2_i, dt, 2, flux_vitesses)
        if key_pshfwd == "lagrangian":
            field1 = (flux_vitesses[0][0][:N, :N] + flux_vitesses[0][0][1:, :N]) / 2, (
                        flux_vitesses[0][1][:N, :N] + flux_vitesses[0][1][:N, 1:]) / 2
            field2 = (flux_vitesses[1][0][:N, :N] + flux_vitesses[1][0][1:, :N]) / 2, (
                        flux_vitesses[1][1][:N, :N] + flux_vitesses[1][1][:N, 1:]) / 2
            rho_1_i = LagrangianPshFwd.pshfwdL(rho_1_i, dt, field1)
            rho_2_i = LagrangianPshFwd.pshfwdL(rho_2_i, dt, field2)

        # Checkup

        if isnan(np.sum(rho_1_i + rho_2_i)) == True:
            raise ValueError('Push Forward step exploded')

        # Total density for step 2

        rho_tot_i = rho_1_i + rho_2_i

        # --------------- STEP 2 : K1 PROJ ------------------

        rho_tot_f = K2_proj.k2_proj(key_proj, rho_tot_i, Ntest)

        # Checkup
        if isnan(np.sum(rho_1_i + rho_2_i)) == True:
            raise ValueError('Projection step exploded')


        # --------------- STEP 3 : K2 PROJ ------------------

        x = np.zeros((2, N * N))
        y = np.zeros((2, N * N))

        # Sinkhorn's data are in row

        mu = np.zeros((N * N))
        nu = np.zeros((N * N))
        for i in range(N):
            for j in range(N):
                x[:, i * N + j] = [i / N, j / N]
                mu[i * N + j] = rho_tot_i[i, j]
                nu[i * N + j] = rho_tot_f[i, j]
        y = np.copy(x)

        # We only keep data where rho is positive in order to prevent Sinkhorn to run computations where there is no mass

        threshold = 0.0001
        A = np.where(mu > threshold)
        mu = mu[A]
        x = x[:, A[0]]

        B = np.where(nu > threshold)
        nu = nu[B]
        y = y[:, B[0]]

        X = torch.FloatTensor(x.T)
        Y = torch.FloatTensor(y.T)
        MU = torch.FloatTensor(mu.T)
        NU = torch.FloatTensor(nu.T)
        C = Variable(Sinkhorn.cost_matrix(X, Y))  # Wasserstein cost function
        l = Sinkhorn.sinkhorn(C, MU, NU, epsilon, N, niter)  # Sinkhorn
        OT_matrix = l[1].numpy()  # Extracting the transport plan

        # Data in line
        rho_1_i = np.reshape(rho_1_i, (N * N))
        rho_1_i = rho_1_i[A]
        rho_2_i = np.reshape(rho_2_i, (N * N))
        rho_2_i = rho_2_i[A]
        rho_tot_i = np.reshape(rho_tot_i, N * N)
        rho_tot_i = rho_tot_i[A]

        # We apply the plan to each density

        rho_1_f = np.dot(rho_1_i / rho_tot_i, OT_matrix)
        rho_2_f = np.dot(rho_2_i / rho_tot_i, OT_matrix)

        # Rebuilding the global densities

        rho_1_i_temp = np.zeros((N * N))
        rho_2_i_temp = np.zeros((N * N))
        rho_1_f_temp = np.zeros((N * N))
        rho_2_f_temp = np.zeros((N * N))
        rho_tot_i_temp = np.zeros((N * N))

        for i in range(np.shape(A)[1]):
            rho_1_i_temp[A[0][i]] = rho_1_i[i]
            rho_2_i_temp[A[0][i]] = rho_2_i[i]
            rho_tot_i_temp[A[0][i]] = rho_tot_i[i]
        for j in range(np.shape(B)[1]):
            rho_1_f_temp[B[0][j]] = rho_1_f[j]
            rho_2_f_temp[B[0][j]] = rho_2_f[j]

        # Square storage

        rho_1_i = np.reshape(rho_1_i_temp, (N, N))
        rho_2_i = np.reshape(rho_2_i_temp, (N, N))

        rho_1_f = np.reshape(rho_1_f_temp, (N, N))
        rho_2_f = np.reshape(rho_2_f_temp, (N, N))
        rho_tot_i = np.reshape(rho_tot_i_temp, (N, N))

        # Checkup

        if isnan(np.sum(rho_1_i + rho_2_i)) == True:
            raise ValueError('Sinkhorn step exploded')

        ## --------------- EXPORT DATA --------------

        if store_results:
            stock.append([rho_tot_f.T, rho_1_f.T, rho_2_f.T])

        if draw_results:

            # Draw rho_1 + rho_2
            fig = plt.figure(figsize=(60, 20))
            ax1 = fig.add_subplot(1, 3, 1)
            ax1.axes.get_xaxis().set_visible(False)
            ax1.axes.get_yaxis().set_visible(False)
            im1 = ax1.imshow(rho_tot_f.T, vmin=0, vmax=1, interpolation='None', origin='lower')
            divider = make_axes_locatable(ax1)
            cax = divider.append_axes('right', size='5%', pad=0.05)
            fig.colorbar(im1, cax=cax, orientation='vertical')

            # Draw rho_1

            ax2 = fig.add_subplot(1, 3, 2)
            ax2.axes.get_xaxis().set_visible(False)
            ax2.axes.get_yaxis().set_visible(False)
            im2 = ax2.imshow(rho_1_f.T, interpolation='None', origin='lower', vmin=0, vmax=1)
            divider = make_axes_locatable(ax2)
            cax = divider.append_axes('right', size='5%', pad=0.05)
            fig.colorbar(im2, cax=cax, orientation='vertical')

            # Draw rho_2
            ax3 = fig.add_subplot(1, 3, 3)
            ax3.axes.get_xaxis().set_visible(False)
            ax3.axes.get_yaxis().set_visible(False)
            im3 = ax3.imshow(rho_2_f.T, interpolation='None', origin='lower', vmin=0, vmax=1)
            divider = make_axes_locatable(ax3)
            cax = divider.append_axes('right', size='5%', pad=0.05)
            fig.colorbar(im3, cax=cax, orientation='vertical')

            # Save results
            fig.savefig('img_vid/last_run/image%s' % str(s))
            plt.close()

    if store_results:
        return stock

if __name__=="__main__":

    # ---------------------------------------------- PARAMETERS --------------------------------------------------------

    N = 80             # Meshsize
    dt = .01           # Timestep
    Ttot = .05         # Final time
    Iterations = int(Ttot / dt)

    ## Method parameters

    epsilon = .0001             # Sinkhorn's regularisation coefficient
    niter = 10                  # Maximal number of iterations in Sinkhorn
    key_proj = 'stoch'          # Choice of K2 projection : 'stoch' for the stochastic projection (recommanded)
                                # 'sand' for the sandpile projection
    key_pshfwd = 'lagrangian'   # Choice of the pushforward scheme : 'lagrangian' for the Lagrangian transport
                                # 'volfin' for the finite volume transport
    Ntest = 50                  # Number of parallel runs of the stochastic projection

    ## Model parameters

    chi = 10            # Chemoattraction parameter
    alpha = 0.          # Inter-type chemoattraction coefficient parameter (for equal attraction regardless of the type: 1)
    xi = 0.             # External potential parameter
    eta= .0             # Interaction parameter
    beta = 0            # Inter-type interaction parameter  (for equal interaction regardless of the type: 1)
    zeta= 0.            # External velocity field parameter
    rPot= 0.04          # Radius of interaction (the space is [0,1]*[0,1])


    # ---------------------------------------------- INITIAL DENSITIES AND FIELDS --------------------------------------

    border = 1/50

    rho_1 = np.ones((N, N))*.05
    rho_2 = np.ones((N, N))*.05
    rho_1[:int(N*border),:] = 0
    rho_1[int((1-border)*N):,:] = 0
    rho_1[:,:int(N*border)] = 0
    rho_1[:,int((1-border)*N):] = 0
    rho_2[:int(N*border),:] = 0
    rho_2[int((1-border)*N):,:] = 0
    rho_2[:,:int(N*border)] = 0
    rho_2[:,int((1-border)*N):] = 0

    r = 1 / 15              # Radius

    for x in [1,2,3]:
        for i in range(N):
            for j in range(N):
                if (j/N - (x+.5)/5) ** 2 + (i/N - (0+0.5) / 5) ** 2 < r ** 2:
                    rho_1[i, j] += .3

    for x in [1,3]:
        for i in range(N):
            for j in range(N):
                if (j/N - (x+.5)/5) ** 2 + (i/N - (1+0.5) / 5) ** 2 < r ** 2:
                    rho_1[i, j] += .3

    for x in [1,2,3]:
        for i in range(N):
            for j in range(N):
                if (j/N - (x+.5)/5) ** 2 + (i/N - (2+0.5) / 5) ** 2 < r ** 2:
                    rho_1[i, j] += .3

    for i in range(N):
        for j in range(N):
            if (j/N - (2+.5)/5) ** 2 + (i/N - (3+0.5) / 5) ** 2 < r ** 2:
                rho_1[i, j] += .3


    for i in range(N):
        for j in range(N):
            if (j/N - (2+.5)/5) ** 2 + (i/N - (1+0.5) / 5) ** 2 < r ** 2:
                rho_2[i, j] += .3




    for x in [1,2,3]:
        for i in range(N):
            for j in range(N):
                if (j/N - (x+.5)/5) ** 2 + (i/N - (2+0.5) / 5) ** 2 < r ** 2:
                    rho_2[i, j] += .3

    for x in [1,3]:
        for i in range(N):
            for j in range(N):
                if (j/N - (x+.5)/5) ** 2 + (i/N - (3+0.5) / 5) ** 2 < r ** 2:
                    rho_2[i, j] += .3

    for x in [1,2,3]:
        for i in range(N):
            for j in range(N):
                if (j/N - (x+.5)/5) ** 2 + (i/N - (4+0.5) / 5) ** 2 < r ** 2:
                    rho_2[i, j] += .3

    ## External field

    def u_1(x, y):
        return (1-x),(1+y)

    def u_2(x, y):
        return (1-x),(1+y)

    ## Constant potential

    "Cells are subject to -\grad(pot)"

    def pot_1(x, y):
        return 2*((y-1/2)**2+(x-1/2)**2)

    def pot_2(x, y):
        return 2*((y-1/2)**2+(x-1/2)**2)

    ## Interaction potential

    "rPot is the maximal interaction range"
    "interaction term of the form \grad(V*rho)"

    def kernel(u,rPot):
        if u <= rPot:
            return rPot**2*35/32*((1-u**2/rPot**2)**3)
        else:
            return 0

    # ---------------------------------------------- MAIN --------------------------------------------------------------

    Splitting(rho_1,rho_2,N, dt, Iterations, u_1, u_2,store_results = False, zeta=zeta, pot_1=pot_1, pot_2=pot_2, xi=xi, chi=chi, alpha=alpha,
              eta=eta, beta=beta, kernel=kernel, rPot = rPot, key_pshfwd="volfin")