import numpy as np

def grad_pot(potentiel_1,potentiel_2):
    ## Computation of - gradients of the potentials
    ## The discretization is a decentered computation

    N=potentiel_1.shape[0]
    dx=1/N
    x_vel_1 = np.zeros((N + 1, N))
    y_vel_1 = np.zeros((N, N + 1))
    x_vel_2 = np.zeros((N + 1, N))
    y_vel_2 = np.zeros((N, N + 1))

    ## To be optimized

    for i in range(N + 1):
        for j in range(N):
            x_vel_1[i, j] = -(potentiel_1[i%N ,j]-potentiel_1[i-1,j])/dx
            y_vel_1[j, i] = -(potentiel_1[j,i%N]-potentiel_1[j,i-1])/dx
            x_vel_2[i, j] = -(potentiel_2[i%N,j]-potentiel_2[i-1,j])/dx
            y_vel_2[j, i] = -(potentiel_2[j,i%N]-potentiel_2[j,i-1])/dx
    return x_vel_1,x_vel_2,y_vel_1,y_vel_2

def flux(u_1,u_2,potentiel_1,potentiel_2, N,zeta):
    ## Computes zeta*u_i - grad(pot_i)

    x_vel_1,x_vel_2,y_vel_1,y_vel_2 = grad_pot(potentiel_1,potentiel_2)

    ## To be optimized

    for i in range(N + 1):
        for j in range(N):
            x_vel_1[i, j] += u_1(i / N, j / N)[0]*zeta
            y_vel_1[j, i] += u_1(j / N, i / N)[1]*zeta
            x_vel_2[i, j] += u_2(i / N, j / N)[0]*zeta
            y_vel_2[j, i] += u_2(j / N, i / N)[1]*zeta

    return [x_vel_1, y_vel_1],[x_vel_2, y_vel_2]


## Upwind operator for finite volume transport

def A_up(u, a, b):
    if u > 0:
        return u * a
    else:
        return u * b

## Operator for finite volume transport

def pas_fwd(rho,ds, k, flux_vitesses):

    ## ds: timestep
    ## k (1 or 2): key of the density

    n = np.shape(rho)[0]
    dx = 1 / n
    V, W = flux_vitesses[k-1]
    rf = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            if j == n - 1 and i == n - 1:
                rf[i, j] = rho[i, j] - ds / dx * (
                        A_up(V[0, j], rho[i, j], rho[0, j]) - A_up(V[i, j], rho[i - 1, j], rho[i, j])) - ds / dx * (
                                   A_up(W[i, 0], rho[i, j], rho[i, 0]) - A_up(W[i, j], rho[i, j - 1], rho[i, j]))
            elif j == n - 1:
                rf[i, j] = rho[i, j] - ds / dx * (
                        A_up(V[i + 1, j], rho[i, j], rho[i + 1, j]) - A_up(V[i, j], rho[i - 1, j],
                                                                           rho[i, j])) - ds / dx * (
                                   A_up(W[i, 0], rho[i, j], rho[i, 0]) - A_up(W[i, j], rho[i, j - 1], rho[i, j]))
            elif i == n - 1:
                rf[i, j] = rho[i, j] - ds / dx * (
                        A_up(V[0, j], rho[i, j], rho[0, j]) - A_up(V[i, j], rho[i - 1, j], rho[i, j])) - ds / dx * (
                                   A_up(W[i, j + 1], rho[i, j], rho[i, j + 1]) - A_up(W[i, j], rho[i, j - 1],
                                                                                      rho[i, j]))
            else:
                rf[i, j] = rho[i, j] - ds / dx * (
                        A_up(V[i + 1, j], rho[i, j], rho[i + 1, j]) - A_up(V[i, j], rho[i - 1, j],
                                                                           rho[i, j])) - ds / dx * (
                                   A_up(W[i, j + 1], rho[i, j], rho[i, j + 1]) - A_up(W[i, j], rho[i, j - 1],
                                                                                      rho[i, j]))
    return rf
