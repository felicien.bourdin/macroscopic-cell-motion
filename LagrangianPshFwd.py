import numpy as np
import matplotlib.pyplot as plt



def pshfwdL(rho,tau,field):
    ## Push forward rho_t = (1+tauU)_# rho with periodic BC

    N=rho.shape[0]

    # Center en of cells positions

    x = np.linspace(0,1-1/N,N)+1/2/N
    y = np.linspace(0,1-1/N,N)+1/2/N

    xx,yy = np.meshgrid(x,y, indexing='ij')

    # We compute the next position of the center

    xx = xx+tau*field[0]
    yy = yy+tau*field[1]

    # We dispatch the cell between the four closest cells
    posx = np.floor(N*xx-1/2+0.000001)
    posy = np.floor(N*yy-1/2+0.000001)
    coefx = N*xx-1/2-posx
    coefy = N*yy-1/2-posy
    rho_cell = (1-coefx)*(1-coefy)*rho
    rho_cell_h = (1-coefx)*coefy*rho
    rho_cell_d = coefx*(1-coefy)*rho
    rho_cell_dh = coefx*coefy*rho
    tab = np.zeros((N,N))




    for k in range(N):
        for l in range(N):
            tab[int(posx[k,l]%N),int(posy[k,l]%N)]+=rho_cell[k,l]
            tab[int((posx[k,l]+1)%N),int(posy[k,l]%N)]+=rho_cell_d[k,l]
            tab[int((posx[k,l]+1)%N),int((posy[k,l]+1)%N)]+=rho_cell_dh[k,l]
            tab[int(posx[k,l]%N),int((posy[k,l]+1)%N)]+=rho_cell_h[k,l]

    ## WAY FASTER BUT BACKPROP NOT AVAILABLE FOR BINCOUNT ....

    # tab2 = torch.tensor(np.zeros(N))
    # tab2 += torch.bincount(pos2%N,weights = rho_cell)
    # tab2 += torch.bincount((pos2+1)%N,weights = rho_cell_d)

    return tab

if __name__=='__main__':
    N = 100
    border = 1 / 50

    rho_1 = np.zeros((N, N)) * .05
    rho_2 = np.zeros((N, N)) * .05

    for i in range(N):
        for j in range(N):
            if (i/N-.5)**2+(j/N-.5)**2 < .2**2:
                rho_1[i,j]+=1

    def u(x,y):
        return np.sign(-x+.5)*(-x+.5)**2,np.sign(-y+.5)*(-y+.5)**2
    x = np.linspace(0,1-1/N,N)+1/(2*N)
    y = np.linspace(0,1-1/N,N)+1/(2*N)
    xx,yy = np.meshgrid(x,y, indexing = 'ij')

    field = u(xx,yy)
    tau = .001
    print(field)
    # print(rho)
    fig1,ax1 = plt.subplots()
    print(xx.shape,yy.shape,field[0].shape,field[1].shape)
    # plt.imshow(rho_1, origin='lower')
    plt.quiver(xx,yy,field[0],field[1])

    plt.colorbar()

    for k in range(10):
        rho_1 = pshfwdL(rho_1,tau,field)

    fig2,ax2 = plt.subplots()
    plt.imshow(rho_1, origin='lower')
    plt.colorbar()
    plt.show()