import numpy as np

## -------------------------------- STOCHASTIC PROJECTION ------------------------------------


def stoch_proj(Ntest,rho_tot):
    rho_tot_i=1.*rho_tot
    rho_mean = np.zeros_like(rho_tot_i)
    N=rho_tot_i.shape[0]
    for k in range(Ntest):
        # We run Ntest and take the average
        # We dispatch randomly the mass above 1 by transporting it by a random walk

        v = np.array([[-1, 0], [1, 0], [0, -1], [0, 1]])
        rho = rho_tot_i.copy()
        rng = np.random.RandomState()
        while True:
            i, j = np.unravel_index(np.argmax(rho, axis=None), (N, N))
            if rho[i, j] <= 1:
                break
            e = rho[i, j] - 1
            while (e > 0):
                rho[i, j] = 1
                shift = rng.randint(4, size=1)
                i = (i + v[shift, 0]) % N
                j = (j + v[shift, 1]) % N
                rho[i, j] += e
                e = rho[i, j] - 1
        rho_mean += rho
    rho_mean /= Ntest
    rho_tot_i = rho_mean
    return rho_tot_i

## -------------------------------- SANDPILE PROJECTION ------------------------------------

"""
This method is not running, I let it because it might be faster than stochastic projection if improved
"""

def sable_proj(rho):
    N = np.shape(rho)[1]
    rho_temp = rho.copy()
    if np.sum(rho_temp) / N / N > 1:
        return 'Total mass too large'
    while True:
        i, j = np.unravel_index(np.argmax(rho_temp), (N, N))
        if rho_temp[i, j] > 1 + 0.01:
            excedent = rho_temp[i, j] - 1
            rho_temp[i, j] = 1
            rho_temp[(i - 1) % N, j] += excedent / 4
            rho_temp[(i + 1) % N, j] += excedent / 4
            rho_temp[i, (j - 1) % N] += excedent / 4
            rho_temp[i, (j + 1) % N] += excedent / 4
        else:
            break
    return (rho_temp)


## ------------------------------------------ PROJECTION -----------------------------------------

def k2_proj(cle,rho_tot,Ntest):
    if cle == 'stoch':
        return(stoch_proj(Ntest,rho_tot))
    if cle == 'sand':
        return sable_proj(rho_tot)