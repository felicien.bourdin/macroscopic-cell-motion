import scipy.sparse as ssp
import numpy as np
import scipy.sparse.linalg
from scipy.ndimage.filters import convolve

## -----------------Matricial differential operators --------------------------

def Lap_mat_2D(N):
    ## Matrix of the Laplace operator in 2D
    dx=1/N
    Id1d = ssp.identity(N)
    ones = np.ones(N)
    diag = 2*ones
    aux = [diag, -ones[:-1], -ones[:-1],[-1],[-1]]
    Lap1d = ssp.diags(aux, [0, -1, 1,-(N-1),N-1])
    Lap2d = ssp.kron(Id1d, Lap1d) + ssp.kron(Lap1d, Id1d)
    return -Lap2d/dx/dx


## --------------- Matricial operations on the potentials ----------------------


def to_mat(pot_1,N):
    ## Converts the potential function into a matrix

    pot_1_m=np.zeros((N,N))

    ## To be optimized
    for i in range(N):
        for j in range(N):
            pot_1_m[i,j]=pot_1(i / N, j / N)
    return pot_1_m


def noyau_to_mat(noyau,N,rPot):
    ## Converts the potential function into a matrix
    dx=1/N
    taille_filtre=2*int(rPot/dx)+1
    filtre=np.zeros((taille_filtre,taille_filtre))
    centre=(taille_filtre-1)/2

    ## To be optimized
    for i in range(taille_filtre):
        for j in range(taille_filtre):
            u=(centre-i)**2+(centre-j)**2
            u/=N**2
            filtre[i,j]=noyau(u,rPot)
    return filtre


def mat_interaction(rho_1,rho_2,noyau,rPot):
    ## Computes V*(rho_1+rho_2)
    N=rho_1.shape[0]
    input=rho_1+rho_2
    filtre=noyau_to_mat(noyau,N,rPot)
    output=scipy.ndimage.filters.convolve(input, filtre, mode='wrap')
    return output

#------------------ Chemoattraction ------------------------------


def pot_chemo(rho_1,rho_2,N,alpha_1=1,alpha_2=1):
    ## We solve a Neumann problem to get the chemoattraction potential
    Lap2d=Lap_mat_2D(N)
    Id = ssp.identity(N * N)
    A1=alpha_1*Id-Lap2d
    B1=rho_1.reshape((N*N))
    A2=alpha_2*Id-Lap2d
    B2=rho_2.reshape((N*N))
    potentiel_1=ssp.linalg.spsolve(A1,B1+B2) #-Lapl=rho
    potentiel_2=ssp.linalg.spsolve(A2,B1+B2)
    potentiel_1=potentiel_1.reshape((N,N))
    potentiel_2=potentiel_2.reshape((N, N))
    return potentiel_1,potentiel_2


if __name__ == '__main__':
    N=100
    rPot=.04
    rho_1=np.ones((N,N))
    rho_2=np.eye(N)
    print(mat_interaction(rho_1,rho_2,N,rPot))